<snippet>
  <content>
# Migrador BlazedWoW
Migrador espejo adaptado de la 3.3.5a a 4.3.4 para BlazedWoW

# Progresos


### Profesiones

- Arqueologia : 50% (Falta agregar habilidades) 
- Desuello : 90% [Desde la ultima commit (ebe498f2) se pueden pasar falta comprobar]
- Peleteria : 90% [Desde la ultima commit (ebe498f2) se pueden pasar falta comprobar] 
- Heboristeria : 90% [Desde la ultima commit (ebe498f2) se pueden pasar falta comprobar] 
- Primeros Auxilios :90% [Desde la ultima commit (c58399cf) se pueden pasar falta comprobar] 
- Cocina : 90% [Desde la ultima commit (fe08f304) se pueden pasar falta comprobar]
- Pesca : 90% [Desde la ultima commit (fe08f304) se pueden pasar falta comprobar]
- Sastrería : 90% [Desde la ultima commit (fe08f304) se pueden pasar falta comprobar]
- Mineria : 90% [Desde la ultima commit (c58399cf) se pueden pasar falta comprobar] 
- Herreria : 90% [Desde la ultima commit (c58399cf) se pueden pasar falta comprobar] 
- Alquimia : 90% [Desde la ultima commit (ff66f08d) se pueden pasar falta comprobar]
- Inscripcion : 0%
- Joyeria : 0%
- Encantamiento : 0%
- Ingenieria : 0%

### Reputaciones 

- Cataclysm : 90% [Desde la ultima commit (2a54c909) se pueden pasar falta comprobar] 
- Gilneas : 100% [Desde la ultima commit (2a54c909) se pueden pasar falta comprobar] 
- Goblins : 100% [Desde la ultima commit (2a54c909) se pueden pasar falta comprobar] 

### Razas (Nuevas)

- Worgens : 90% [Desde la ultima commit (0e3a91d9) se pueden pasar falta comprobar]
- Goblins : 90% [Desde la ultima commit (0e3a91d9) se pueden pasar falta comprobar]

### Clases

- Guerrero : 0%
- Brujo : 0%
- Mago : 0%
- Sacerdote : 0%
- Caballero de la muerte : 0%
- Paladin : 0%
- Cazador : 0%
- Picaro : 0%
- Chaman ; 0%

### Hechizos

- Guerrero : 0%
- Brujo : 0%
- Mago : 0%
- Sacerdote : 0%
- Caballero de la muerte : 0%
- Paladin : 0%
- Cazador : 0%
- Picaro : 0%
- Chaman ; 0%

### Glifos 

- Guerrero : 0%
- Brujo : 0%
- Mago : 0%
- Sacerdote : 0%
- Caballero de la muerte : 0%
- Paladin : 0%
- Cazador : 0%
- Picaro : 0%
- Chaman ; 0%

### Misiones

- Misiones del DK : 100% (Comprobacion en espera )
- Misiones de Hijos de odir ; 100% (Comprobacion en espera )

### Equitaciones

- Equitaciones : 100% (Terminada y Adaptada a 4.3.4)

### Gemas

- Gemas : 1% [Desde la ultima commit (ff66f08d) se estan implementando]


# Contribuir

1. Clonar la rama
2. Documentaros bien en lo que cambieís
3. Commitear el cambio de la siguiente forma "[Rama 4.3.4/5.0.5] Agregada las profesiones de (Sastreria)"
4. Pushea el cambio en la branch correspondiente 4.3.4/5.0.5
5. Y listo !


# Creditos

- AresBW : Codigo y Adaptación


</snippet>